
//take data from json file
function main() {
    fetch('../../content/restaurant.json').then(response => {
        return response.json();
      }).then(datas => {
        
       
            for (data in datas) {
            
                let name = datas[data].restaurant;
                let adress = datas[data].address;
                let ratings = datas[data].ratings;

                //Add markers on the map
                let marker = L.marker([
                    datas[data].lat,
                    datas[data].long 
                    ]);
                    marker.addTo(map)
                    addPopup(marker, name, ratings)


                //Add datas on table
                createHtmlTable(name, adress, ratings, marker)
            }
            //calculate medium notes
            document.getElementsByClassName(name).innerHTML = mediumNotes(ratings)
      }).catch(err => {
      });
}

//add event on add-restaurant button
function addRestaurant() {
    
    document.querySelector('#add-restaurant').addEventListener('click', function (e) {
    let addMessage = document.querySelector('.add-message-resto')
    console.log(addMessage)
    let messageHtml = `<h3> Veuillez clicker sur la carte pour créer un nouveau restaurant </h3>`
    addMessage.innerHTML=messageHtml;

    map.on('click', function(e){
        console.log(e)
        var marker = new L.marker(e.latlng).addTo(map);
    });

})
}


//add content on popup marker (name and comments)
function addPopup(marker, name, ratings) {
    let popupHtml = `<h3> <strong>${name}</strong></h3>`
    for (rating in ratings) {
        popupHtml+=`
            <div> 
                <h5><strong>Commentaire:</strong> ${ratings[rating].comment}</h5>
                <h5><strong>Rating:</strong> ${ratings[rating].stars} </h5>
                </br>
            </div>
        `
    }
    marker.bindPopup(popupHtml);
    popupHtml = ""
}

//zoom in to selected marker
function zoomOnMarker(marker, zoom) {
    let latLngs = [marker.getLatLng()];
    // let markerBounds = L.latLngBounds(latLngs);
    map.setView([latLngs[0].lat, latLngs[0].lng], zoom);
}

//table creation into the DOM with restaurant list and related datas
function createHtmlTable(name, adress, ratings, marker) {
   
    let mediumNote = mediumNotes(ratings)
    let dom_restaurants = document.getElementById("restaurants")
    new_dom_restaurant = document.createElement('div');
    new_dom_restaurant.classList.add("restaurant");

    new_dom_restaurant.innerHTML = `
        <header>
            <nav>
                <img class="photoRestaurant" src="" alt="">
            </nav>
        </header>
        <div class="infosRestaurant">
            <h4>${name}</h4>
            <h6>${adress}</h6>
            

        </div>
        <div class="button-comment">
           <button>Ajouter un commentaire</button>
        </div>
       
        <div class="insert-input"></div>
       `
    let str = `<i class="fas fa-star hostingMainCardGradeIcon" data-active="true"></i>`
    new_dom_restaurant.innerHTML += ` <aside class="stars">`+ `<h4>${mediumNote}</h4>`+str.repeat(parseInt(mediumNote)) + `</aside>`
    new_dom_restaurant.addEventListener('click', function(e){
        marker.fire('click')

        //saving datas for creation of newpopup
        localStorage.clear();
    
        localStorage.setItem('lat', marker.getLatLng().lat)
        localStorage.setItem('lng', marker.getLatLng().lng)
        localStorage.setItem('ratings', JSON.stringify(ratings))
        localStorage.setItem('name', name )
        zoomOnMarker(marker, 18)


    })
    dom_restaurants.append(new_dom_restaurant);
    

    //Add comment button
    let insertInput = new_dom_restaurant.getElementsByClassName("insert-input")[0]
    new_dom_restaurant.querySelector('.button-comment').addEventListener('click', function(e){
        
        //hide add comment button when clicked
        e.target.style.display ='none';

        //creation of form new comment
        let formDiv = document.createElement("div")
        formDiv.setAttribute('class', 'addComment')
        
        let form = document.createElement("form")
        
        let newInput = document.createElement('input')
        newInput.type = "text"
        newInput.placeholder = "Entrez un commentaire";
        newInput.name = "comment"
       

        let newRating = document.createElement('input')
        newRating.type = "number";
        newRating.placeholder= "entrez une note"
        newRating.name = "rating"

        let submit = document.createElement('button')
        submit.type = "submit";
        submit.innerHTML = "Envoyer le commentaire"

        //Comment form display
        insertInput.append(formDiv)
        formDiv.append(form)
        form.append(newInput)
        form.append(newRating)
        form.append(submit)

        form.addEventListener('submit', (e) => {
            e.preventDefault()
            submitComment(e)
        })
    });

}
//Add restaurant comment
function submitComment(e) {
   
        let lat = parseFloat(localStorage.getItem('lat'))
        let lng =  parseFloat(localStorage.getItem('lng'))
        let ratings = JSON.parse(localStorage.getItem('ratings'))
        let name = localStorage.getItem('name')
        e.target.style.display = 'none'

        //taking datas from the form
        const data = Object.fromEntries(new FormData(e.target).entries());
        let newComment = {'stars': data["rating"], 'comment':data["comment"]}
        ratings.push(newComment)
        //creation of new marker with new comments
        let newMarker = L.marker([
            lat,
            lng 
            ]);
          
        addPopup(newMarker, name, ratings)
        newMarker.addTo(map)
        newMarker.fire('click')
        localStorage.clear()
}
//calculate the medium note of a restaurant
function mediumNotes (ratings) {
    let nbNotes = 0;
    let totalNotes = 0;
    for (rating in ratings) {
        nbNotes += 1
        totalNotes+=ratings[rating].stars
    }
    return totalNotes/nbNotes;
}


//Leaflet layer building

let map = L.map('map').setView([48.85, 2.36], 11);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZ2FneWdhZ3k3NiIsImEiOiJja3gwNnVwMnExMnVwMnZwOGZicHhicTlsIn0.Du10zptrHU-vwwI_SDUMSQ', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiZ2FneWdhZ3k3NiIsImEiOiJja3gwNnVwMnExMnVwMnZwOGZicHhicTlsIn0.Du10zptrHU-vwwI_SDUMSQ'
}).addTo(map);

addRestaurant()
main()
